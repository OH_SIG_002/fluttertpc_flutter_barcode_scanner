## flutter_barcode_scanner_ohos

flutter_barcode_scanner_ohos is a plugin for Flutter apps that adds barcode scanning support on Ohos.

## How to use ?

To use on ohos, you will need to add the camera permission in module.json5.

```
"requestPermissions": [
      {
        "name": "ohos.permission.CAMERA",
        "reason": "$string:reason",
        "usedScene": {
          "abilities": [
            "EntryAbility"
          ],
          "when": "inuse"
        }
      }
    ]
```


After making the changes in Ohos add flutter_barcode_scanner to `pubspec.yaml`
```  
dependencies:
  ...
  flutter_barcode_scanner: ^2.0.0
```
## Usage

```
  yaml
    dependencies:
      flutter_barcode_scanner: 2.0.0
      flutter_barcode_scanner_ohos: 1.0.0
 ```

### One time scan
1. You need to import the package first.

```
import 'package:flutter_barcode_scanner_ohos/flutter_barcode_scanner.dart';
```


2. Then use the `scanBarcode` method to access barcode scanning.

```
String barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
                                                    COLOR_CODE, 
                                                    CANCEL_BUTTON_TEXT, 
                                                    isShowFlashIcon, 
                                                    scanMode);
```

Here in `scanBarcode`,

`COLOR_CODE` is hex-color which is the color of line in barcode overlay you can pass color of your choice,

`CANCEL_BUTTON_TEXT` is a text of cancel button on screen you can pass text of your choice and language,

`isShowFlashIcon` is bool value used to show or hide the flash icon,

`scanMode` is a enum in which user can pass any of `{ QR, BARCODE, DEFAULT }`, if nothing is passed it will consider a default value which will be `QR`.
It shows the graphics overlay like for barcode and QR.

NOTE: Currently, `scanMode` is just to show the graphics overlay for barcode and QR. Any of this mode selected will scan both QR and barcode.

### Continuous scan
* If you need to scan barcodes continuously without closing camera use `FlutterBarcodeScanner.getBarcodeStreamReceiver`
  params will be same like `FlutterBarcodeScanner.scanBarcode`
  e.g.


```
FlutterBarcodeScanner.getBarcodeStreamReceiver("#ff6666", "Cancel", false, ScanMode.other)
         .listen((barcode) { 
         /// barcode to be used
         });
```
